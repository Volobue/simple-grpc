package main

import (
	"context"
	"fmt"
	"gRPC_Example/internal/config"
	"gRPC_Example/internal/repository/mongo"
	"gRPC_Example/internal/services"
	"gRPC_Example/proto"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"net"
)

var (
	host = "localhost"
	port = "5000"
)

func main() {
	ctx := context.Background()

	err := setUpViper()

	if err != nil {
		log.Fatalf("Error reading yml file: %v", err)
	}

	addr := fmt.Sprintf("%s:%s", host, port)

	lis, err := net.Listen("tcp", addr)

	if err != nil {
		log.Fatalf("Error starting tcp listener: %v", err)
	}

	mongoDataBase, err := config.SetUpMongoDataBase(ctx)

	if err != nil {
		log.Fatalf("Error starting mongo: %v", err)
	}

	userRepository := mongo.NewUserRepository(mongoDataBase.Collection("users"))
	userService := services.NewUserService(userRepository)

	grpcServer := grpc.NewServer()

	proto.RegisterUserServiceServer(grpcServer, userService)

	log.Printf("gRPC started at %v\n", port)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Error starting gRPC %v", err)
	}

}

func setUpViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	return nil
}
